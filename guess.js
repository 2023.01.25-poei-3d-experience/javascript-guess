const readlineSync = require('readline-sync');

// la machine choisit le nombre à deviner
const solution = Math.floor(Math.random()*100);
let remainingTries = 7;
console.log(solution);

// tant que l'utilisateur n'a pas deviné
let guess;
do {
    // lire la proposition de l'utilisateur
    guess = parseInt(readlineSync.question("your guess ? "));
    // sinon si essai < nombre => trop petit, recommence
    if (guess < solution)
        console.log("too small, try a higher value.");
    // sinon => trop grand, recommence
    else if (guess > solution)
        console.log("too big, try a smaller value.");
} while (solution !== guess && --remainingTries > 0);

if (guess == solution)
        console.log("congrats ! you find the secret number !");
else
    console.log("no more tries ... The machine won ! The correct answer was " + solution);